# PgBouncer
**Проблема**
* Не эффективное использование ОЗУ сервера БД. Примерно от 1.5 до 14.5 MB выделяется на соеденение. Если будет откруто 100 соеденений к БД, на это потребуется ~1.45GB ОЗУ сервера БД
    * При этом полезное использование соеденения < 50%. Если открытие соеденение используется для выполнения запроса половину времени.
* Затрата времени на переоткрытие соеденений от ППО к БД
    * TLS авторизация Hand shake
* Микросервисная архитектура приводит к сложным рассчетам/управлению и контролю объема пуллов соеденений к БД на уровне ППО
* Управления пуллом соеденений к БД на уровне ППО происходит внутри ППО. В случаях деградации или сбоях ППО может оставаться больше количество открытых не используемых соеденений

**Почему PgBouncer**
* **Pooling Modes** – By giving users the power to decide when a connection is returned to the pool, PgBouncer is able to support a vast range of use cases
* **Easy Setup & Use** – PgBouncer is one of the easiest PostgreSQL connection poolers to setup out of the box, and it also requires no client-side code changes.
* **Passthrough Authentication** – PgBouncer is one of the few “middleware” connection poolers that can securely authenticate a user without having access to their passwords
* **Lightweight** – It is a single process, and all commands from the client and responses from the server passthrough PgBouncer without any processing.
* **Scalability & Performance** – As we will discuss in more detail in the final part of our series, PgBouncer can significantly improve the transactions per second that your PostgreSQL server can support, and it scales very well to a very large number of clients.

***Примечание***
PgBouncer, while a great connection pooler, does not support automated load balancing or high-availability. It recommends using other common linux tools like HAProxy

**Настройки**
* Pool name - type a name for your new pool. This name becomes the database dbname connection parameter for your pooled client connectons.
* Database - field, select a database to connect to. Each pool can only connect to one database.
* Pool Mode
* Pool Size - select the maximum number of server connections this pool can use at any one time.
* Username - select which database username to connect to the database with.

**Режим работы пулла(Pool Mode)**
Одна из важных настроек это является настройка пулла, как именно PgBouncer переиспользует соеденения к БД. There are several different pool modes:
* Transaction (по умолчанию)
* Session
* Statement


**Литература**
* [Postgrespro pgbouncer](https://postgrespro.ru/docs/postgrespro/10/pgbouncer)
* [PostgreSQL Connection Pooling: Part 2 – PgBouncer](https://scalegrid.io/blog/postgresql-connection-pooling-part-2-pgbouncer/)