https://github.com/apache/activemq/blob/main/activemq-client/src/main/java/org/apache/activemq/ActiveMQMessageConsumer.java#L497 

https://activemq.apache.org/maven/apidocs/org/apache/activemq/jms/pool/PooledConnectionFactory.html

https://activemq.apache.org/what-is-the-prefetch-limit-for

https://stackoverflow.com/questions/51842587/apache-activemq-artemis-equivalent-of-prefetch-policy 

a problem with Prefetch policy. What is the Artemis equivalent of the following code:

ActiveMQPrefetchPolicy prefetchPolicy = new ActiveMQPrefetchPolicy();
prefetchPolicy.setQueuePrefetch(0);
prefetchPolicy.setQueueBrowserPrefetch(0); 
connectionFactory.setPrefetchPolicy(prefetchPolicy);

is

Artemis supports OpenWire so I believe you should be able to keep using your old code. However, if you want to change for whatever reason the equivalent property is consumerWindowSize and it can be set on the URL used to connect to the broker, e.g. tcp://host:61616?consumerWindowSize=0.

https://developer.jboss.org/thread/261859

<subsystem xmlns="urn:jboss:domain:resource-adapters:3.0">
            <resource-adapters>
                <resource-adapter id="activemq-rar">
                    <archive>
                        activemq-rar-5.11.1.rar
                    </archive>
                    <transaction-support>XATransaction</transaction-support>
                    <config-property name="ServerUrl">
                        tcp://192.168.1.127:61616?jms.rmIdFromConnectionId=true&amp;jms.prefetchPolicy.queuePrefetch=1
                    </config-property>
                    <config-property name="UserName">
                        admin
                    </config-property>
                    <config-property name="UseInboundSession">
                        false
                    </config-property>
                    <config-property name="Password">
                        admin
                    </config-property>
                    <connection-definitions>
                        <connection-definition class-name="org.apache.activemq.ra.ActiveMQManagedConnectionFactory" jndi-name="java:jboss/DefaultJMSConnectionFactory" enabled="true" pool-name="ConnectionFactory">
                            <xa-pool>
                                <min-pool-size>1</min-pool-size>
                                <max-pool-size>20</max-pool-size>
                                <prefill>false</prefill>
                                <is-same-rm-override>false</is-same-rm-override>
                            </xa-pool>
                        </connection-definition>
                    </connection-definitions>
                    <admin-objects>
                        <admin-object class-name="org.apache.activemq.command.ActiveMQQueue" jndi-name="java:/queue/HELLOWORLDMDBQueue" use-java-context="true" pool-name="HELLOWORLDMDBQueue">
                            <config-property name="PhysicalName">
                                HELLOWORLDMDBQueue
                            </config-property>
                        </admin-object>
                        <admin-object class-name="org.apache.activemq.command.ActiveMQTopic" jndi-name="java:/topic/HELLOWORLDMDBTopic" use-java-context="true" pool-name="HELLOWORLDMDBTopic">
                            <config-property name="PhysicalName">
                                HELLOWORLDMDBTopic
                            </config-property>
                        </admin-object>
                    </admin-objects>
                </resource-adapter>
            </resource-adapters>
        </subsystem>